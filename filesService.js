const fs = require('fs');
const path = require('path');

const extensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];

const errorHandler = function (param, res) {
  return res.status(400).json({ 'message': `Please specify ${param} parameter` });
}

const createFile = function (req, res, next) {
  if (!req.body.filename) return errorHandler('filename', res);
  else if (!req.body.content) return errorHandler('content', res);
  else if (!extensions.includes(path.extname(req.body.filename))) return errorHandler('valid file extension', res);

  const filePath = path.join(__dirname, 'files', req.body.filename);

  if (fs.existsSync(filePath)) return res.status(400).json({ 'message': `File already exists` });

  const file = fs.writeFileSync(filePath, req.body.content)

  return res.status(200).json({ 'message': 'File created successfully' });
};

const getFiles = function (req, res, next) {
  const files = fs.readdirSync(path.join(__dirname, 'files'));

  return res.status(200).json({
    "message": "Success",
    "files": files
  });
}

const getFile = function (req, res, next) {
  const filePath = path.join(__dirname, 'files', req.params.filename);

  if (!fs.existsSync(filePath)) {
    return res.status(400).json({ 'message': `No file with '${req.params.filename}' filename found` });
  }

  const file = fs.readFileSync(filePath, { encoding: 'utf-8' }),
    uploadedDate = fs.statSync(filePath).mtime;

  return res.status(200).json({
    "message": "Success",
    "filename": req.params.filename,
    "content": file ?? 'There is no content in this file',
    "extension": path.extname(filePath).replace('.', '') ?? 'There is no extension for this file',
    "uploadedDate": uploadedDate ?? 'There is no uploaded date for this file'
  });
}

const editFile = function (req, res, next) {
  if (!req.body.content) return specifyParameterLog('content', res);

  const filePath = path.join(__dirname, 'files', req.params.filename);

  if (!fs.existsSync(filePath)) {
    return res.status(400).json({ 'message': `No file with '${req.params.filename}' filename found` });
  }

  fs.writeFileSync(filePath, req.body.content);

  return res.status(200).json({ 'message': 'File updated successfully' });
}

const modifyFile = function (req, res, next) {
  if (!req.body.content) return errorHandler('content', res);

  const filePath = path.join(__dirname, 'files', req.params.filename);

  if (!fs.existsSync(filePath)) {
    return res.status(400).json({ 'message': `No file with '${req.params.filename}' filename found` });
  }

  fs.appendFileSync(filePath, req.body.content);

  return res.status(200).json({ 'message': 'File modified successfully' });
}

const deleteFile = function (req, res, next) {
  const filePath = path.join(__dirname, 'files', req.params.filename);

  if (!fs.existsSync(filePath)) {
    return res.status(400).json({ 'message': `No file with '${req.params.filename}' filename found` });
  }

  fs.unlinkSync(filePath);

  return res.status(200).json({ 'message': 'File deleted successfully' });
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  modifyFile,
  deleteFile
}
